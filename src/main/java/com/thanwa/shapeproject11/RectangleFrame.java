/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.shapeproject11;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author tud08
 */
public class RectangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblWide = new JLabel("Wide: ", JLabel.TRAILING);
        lblWide.setSize(50, 20);
        lblWide.setLocation(5, 5);
        lblWide.setBackground(Color.ORANGE);
        lblWide.setOpaque(true);
        frame.add(lblWide);
        
        JLabel lblLong = new JLabel("Long: ", JLabel.TRAILING);
        lblLong.setSize(50, 20);
        lblLong.setLocation(5, 40);
        lblLong.setBackground(Color.ORANGE);
        lblLong.setOpaque(true);
        frame.add(lblLong);
        
        final JTextField txtWide = new JTextField();
        txtWide.setSize(100, 20);
        txtWide.setLocation(60, 5);
        frame.add(txtWide);
        
        final JTextField txtLong = new JTextField();
        txtLong.setSize(100, 20);
        txtLong.setLocation(60, 40);
        frame.add(txtLong);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(200, 40);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Rectangle Wide= ??? Long=??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.PINK);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                String strWide = txtWide.getText();
                double Wide = Double.parseDouble(strWide);
                String strLong = txtLong.getText();
                double Long = Double.parseDouble(strLong);
                Rectangle rectangle = new Rectangle(Wide, Long);
                lblResult.setText("Rectangle Wide = " + String.format("%.2f", rectangle.getWide())
                        + " Long = " + String.format("%.2f", rectangle.getLong())
                        + " area = " + String.format("%.2f", rectangle.calArea())
                        + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtWide.setText("");
                    txtWide.requestFocus();
                    txtLong.setText("");
                    txtLong.requestFocus();
                }
            }
        });
        
        frame.setVisible(true);
    }
}
