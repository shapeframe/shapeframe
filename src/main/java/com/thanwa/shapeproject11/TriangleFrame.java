/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.shapeproject11;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author tud08
 */
public class TriangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblBase = new JLabel("Base: ", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.ORANGE);
        lblBase.setOpaque(true);
        frame.add(lblBase);
        
        JLabel lblHigh = new JLabel("High: ", JLabel.TRAILING);
        lblHigh.setSize(50, 20);
        lblHigh.setLocation(5, 40);
        lblHigh.setBackground(Color.ORANGE);
        lblHigh.setOpaque(true);
        frame.add(lblHigh);
        
        final JTextField txtBase = new JTextField();
        txtBase.setSize(100, 20);
        txtBase.setLocation(60, 5);
        frame.add(txtBase);
        
        final JTextField txtHigh = new JTextField();
        txtHigh.setSize(100, 20);
        txtHigh.setLocation(60, 40);
        frame.add(txtHigh);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(200, 40);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Triangle base= ??? high=??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.PINK);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                String strBase = txtBase.getText();
                double base = Double.parseDouble(strBase);
                String strHigh = txtHigh.getText();
                double high = Double.parseDouble(strHigh);
                Triangle triangle = new Triangle(base, high);
                lblResult.setText("Triangle base = " + String.format("%.2f", triangle.getBase())
                        + " high = " + String.format("%.2f", triangle.getHigh())
                        + " area = " + String.format("%.2f", triangle.calArea())
                        + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHigh.setText("");
                    txtHigh.requestFocus();
                }
            }
        });
        
        frame.setVisible(true);
    }
}
