/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.shapeproject11;

/**
 *
 * @author tud08
 */
public class Rectangle extends Shape{
    
    private double Wide;
    private double Long;

    public Rectangle( double Wide, double Long) {
        super("Rectangle");
        this.Long = Long;
        this.Wide = Wide;
    }

    public double getWide() {
        return Wide;
    }

    public void setWide(double Wide) {
        this.Wide = Wide;
    }

    public double getLong() {
        return Long;
    }

    public void setLong(double Long) {
        this.Long = Long;
    }

    @Override
    public double calArea() {
        return Wide * Long;
    }

    @Override
    public double calPerimeter() {
        return Wide * Long * 2;
    }
    
}
